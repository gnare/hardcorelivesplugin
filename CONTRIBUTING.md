Please fork the repo, and make a Merge Request to `dev` when you are ready to submit your changes.

Your changes will be reviewed to assess their need to be implemented, and we will add more commits to help test them
and integrate them into the project.

I will make sure to credit all contributors to the project on the official Spigot page and the releases the contribute to.

You will need at least Maven 3 and OpenJDK 17 installed to contribute to this project.
