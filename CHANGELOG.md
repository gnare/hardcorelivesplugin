# HardcoreLives Changelog
**1.4**
 - Tested for Minecraft and Spigot 1.20.2
 - Added maxLives and addLives commands (contributors: `jbagwell27`)
 - Fixed a bug where life counts could be set to be negative
 - Configured for OSS Sonatype Deployment under `com.voxelbuster` group ID.
 - Code quality changes and added usage of Lombok.
 - ***You will need to delete your config.json and reboot the server to regenerate it.***

**1.3**
 - Tested for Minecraft and Spigot 1.19.2
 - Added Placeholders for a player's `%hardcorelivesplugin_max_lives%` and current `%hardcorelivesplugin_lives%`
 - Added a config value `loseLifeOnPvpOnly`, defaults to false.
 - Fixed a bug where `hl resetAll` would not always move player data when renaming the player data folder.
 - General code cleanup and stability changes

**1.2**
 - Tested for Minecraft and Spigot 1.16
 - Revamped commands and added command tab completion where applicable.
 - Added custom events for certain parts of the plugin.
 - Added a manual save as well as auto save feature for player data.
 - Reloading no longer overwrites the config by default. `/hl reload` now has an option to 
 manually save the config.
 - The config will automatically save upon updating it with the `/hl config` command.
 - ***You will need to delete your config.json and reboot the server to regenerate it.***

**1.1**
 - Fixed a bug where config was stored in an extra YML file but in JSON format.
 - Introduced buying and selling lives
 - Introduced the ability to give lives to other players
 - Made the `hardcorelives.bypass` permission fully bypass life tracking and game mode changes due to life count.
 - Added Vault integration
 - Added MC Scoreboard support for life tracking
 - Plugin now loads `POSTWORLD` as MC Scoreboard is not ready until after the world is loaded.
 - ***You will need to delete your config.json and reboot the server to regenerate it.***