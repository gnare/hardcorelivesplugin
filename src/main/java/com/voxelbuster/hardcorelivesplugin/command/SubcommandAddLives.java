package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.event.PlayerLifeCountChangedEvent;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SubcommandAddLives extends PluginSubcommand {
    protected SubcommandAddLives(HardcoreLivesPlugin plugin) {
        super("addLives", Optional.of("hardcorelives.setLives"), plugin);
        this.description = "Add x amount of lives to a single player.";
        this.usage = "/hl addLives <player> <number>";
        this.aliases = Arrays.asList("addlives", "al", "add");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length != 2) {
            sendUsageMessage(sender);
            return false;
        }

        if (args[0].equals("@a")) {

            for (OfflinePlayer offlinePlayer : configManager.getLoadedPlayers()) {
                Player player = plugin.getServer().getPlayer(offlinePlayer.getUniqueId());

                ConfigManager.PlayerData data = configManager.getPlayerData(player);

                data.setLives(data.getLives() + Integer.parseInt(args[1]));

                plugin.getServer().getPluginManager()
                    .callEvent(new PlayerLifeCountChangedEvent(player, data.getLives()));
                plugin.updateScoreboard();
            }

            return true;
        }

        Player p = plugin.getServer().getPlayer(args[0]);
        if (p == null) {
            sender.sendMessage(ChatColor.RED + "Player not found.");
            return true;
        }
        ConfigManager.PlayerData data = configManager.getPlayerData(p);
        data.setLives(data.getLives() + Integer.parseInt(args[1]));
        plugin.getServer().getPluginManager()
            .callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
        plugin.updateScoreboard();

        return true;
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            return List.of();
        }

        ArrayList<String> playerNames = new ArrayList<>();
        plugin.getServer()
            .getOnlinePlayers()
            .forEach(p -> playerNames.add(p.getName()));

        playerNames.add("@a");
        return playerNames.stream()
            .filter(playerName -> playerName.startsWith(args[0]))
            .toList();

    }
}
