package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

public class SubcommandSave extends PluginSubcommand {
    protected SubcommandSave(HardcoreLivesPlugin plugin) {
        super("save", Optional.of("hardcorelives.save"), plugin);
        this.description = "Saves all player data for the plugin.";
        this.usage = "/hl save";
        this.aliases = Collections.singletonList("save");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        try {
            plugin.getConfigManager().saveAllPlayers(plugin.getPlayersDir());
        } catch (IOException e) {
            sender.sendMessage(ChatColor.RED + "Saving player data failed!");
            log.log(Level.SEVERE, "Saving player data failed!", e);
            return false;
        }

        sender.sendMessage(ChatColor.GREEN + "Saving complete.");
        return true;
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return List.of();
    }
}
