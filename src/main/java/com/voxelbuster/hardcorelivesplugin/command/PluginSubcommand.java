package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public abstract class PluginSubcommand {
    @Getter
    protected final String name;
    protected final HardcoreLivesPlugin plugin;
    protected final Logger log;

    @Getter
    protected String description;
    @Getter
    protected String usage;

    @Getter
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    protected Optional<String> permissionNode;

    @Getter
    protected List<String> aliases;

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    protected PluginSubcommand(@NotNull String name, Optional<String> permissionNode, HardcoreLivesPlugin plugin) {
        this.name = name;
        this.plugin = plugin;
        this.permissionNode = permissionNode;
        this.log = plugin.getLogger();
    }

    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (!aliases.contains(alias.toLowerCase())) {
            return false;
        }

        if (permissionNode.isPresent() && !PermissionUtil.hasPermission(sender, permissionNode.get())) {
            sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
            return false;
        }

        return subcommandExecute(sender, alias, args);
    }

    public abstract boolean subcommandExecute(CommandSender commandSender, String alias, String[] args);

    public List<String> tabComplete(CommandSender sender, String alias, String[] args) {
        if (!aliases.contains(alias.toLowerCase())) {
            return List.of();
        }

        return subCommandTabComplete(sender, alias, args);
    }

    public abstract List<String> subCommandTabComplete(CommandSender sender, String alias,
                                                       String[] args) throws IllegalArgumentException;

    public void sendUsageMessage(CommandSender sender) {
        sender.sendMessage(ChatColor.GOLD + "Usage: " + ChatColor.GREEN + usage);
    }

}
