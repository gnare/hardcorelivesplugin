package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static org.bukkit.ChatColor.*;

public class SubcommandHelp extends PluginSubcommand {
    protected SubcommandHelp(HardcoreLivesPlugin plugin) {
        super("help", Optional.empty(), plugin);
        this.description = "Show this help message.";
        this.usage = "/hl help" + YELLOW + " [subcommand]";
        this.aliases = Arrays.asList("help", "h");
    }

    @Override
    @SuppressWarnings("squid:S3516") // parent method can return false
    public boolean subcommandExecute(CommandSender commandSender, String alias, String[] args) {
        if (args.length < 1) {
            commandSender.sendMessage(new String[]{
                AQUA + "/hl lives",
                AQUA + "/hl scoreboard",
                AQUA + "/hl lives buy " + GREEN + "<number>" +
                    ((plugin.getConfigManager().getConfig()
                        .isAllowBuyingLives()) ? "" : RED + " : DISABLED"),
                AQUA + "/hl lives sell " + GREEN + "<number>" +
                    ((plugin.getConfigManager().getConfig()
                        .isAllowBuyingLives()) ? "" : RED + " : DISABLED"),
                AQUA + "/hl lives give " + GREEN + "<player> <number>" +
                    ((plugin.getConfigManager().getConfig()
                        .isAllowBuyingLives()) ? "" : RED + " : DISABLED"),
                AQUA + "/hl scoreboardSlot " + GREEN + "<SIDEBAR/PLAYER_LIST/BELOW_NAME>" +
                    WHITE + " : Admin Only",
                AQUA + "/hl lives " + YELLOW + "[player]" + WHITE + " : Admin Only",
                AQUA + "/hl resetAll " + WHITE + " : Admin Only",
                AQUA + "/hl reset " + GREEN + "<player>" + WHITE + " : Admin Only",
                AQUA + "/hl reload " + YELLOW + "[save_config (default=false)]" + WHITE + " : Admin Only",
                AQUA + "/hl save " + WHITE + " : Admin Only",
                AQUA + "/hl autosave " + GREEN + "<number> <time_interval>" + WHITE + " : Admin Only",
                AQUA + "/hl config " + GREEN + "<key> <value>" + WHITE + " : Admin Only",
                AQUA + "/hl setLives " + GREEN + "<player> <lives>" + WHITE + " : Admin Only",
                AQUA + "/hl addLives " + GREEN + "<player> <lives>" + WHITE + " : Admin Only",
                AQUA + "/hl setMax " + GREEN + "<player> <maxLives>" + WHITE + " : Admin Only",
            });

            sendUsageMessage(commandSender);

            return true;
        }

        PluginBaseCommand.SubCommandId subCommandId =
            PluginBaseCommand.SubCommandId.subCommandByAlias(args[0].toLowerCase());

        StringBuilder aliasesSb = new StringBuilder();
        Iterator<String> aliasesIterator = aliases.iterator();

        while (aliasesIterator.hasNext()) {
            aliasesSb.append(GREEN).append(aliasesIterator.next()).append(GOLD);

            if (aliasesIterator.hasNext()) {
                aliasesSb.append(", ");
            }
        }

        PluginSubcommand subcommand = subCommandId.getSubcommand().apply(plugin);
        commandSender.sendMessage(GOLD + "Help for " + GREEN + subcommand.name);
        commandSender.sendMessage(WHITE + subcommand.description);
        commandSender.sendMessage(GOLD + "Aliases: " + aliasesSb);
        commandSender.sendMessage(GOLD + "Usage: " + GREEN + subcommand.usage);

        return true;
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length == 0) {
            return List.of();
        }

        if (args.length > 1) {
            return List.of();
        }

        return PluginBaseCommand.SubCommandId
            .getAllAliases()
            .stream()
            .filter(s -> s.startsWith(args[0].toLowerCase()))
            .toList();
    }
}
