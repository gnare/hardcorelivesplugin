package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.event.PlayerLifeCountChangedEvent;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SubcommandSetMax extends PluginSubcommand {
    protected SubcommandSetMax(HardcoreLivesPlugin plugin) {
        super("setMax", Optional.of("hardcorelives.setMaxLives"), plugin);
        this.description = "Set the maximum life a player can have.";
        this.usage = "/hl setMax <player> <number>";
        this.aliases = Arrays.asList("setmax", "sm");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();

        if (args.length != 2) {
            sendUsageMessage(sender);
            return false;
        }

        Player p = plugin.getServer().getPlayer(args[0]);
        if (p == null) {
            sender.sendMessage(ChatColor.RED + "Player not found.");
            return true;
        }

        ConfigManager.PlayerData data = configManager.getPlayerData(p);
        data.setMaxLives(Integer.parseInt(args[1]));

        plugin.getServer()
            .getPluginManager()
            .callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
        plugin.updateScoreboard();
        return true;
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            return List.of();
        }

        ArrayList<String> playerNames = new ArrayList<>();

        plugin.getServer()
            .getOnlinePlayers()
            .forEach(p -> playerNames.add(p.getName()));

        return playerNames.stream()
            .filter(s -> s.startsWith(args[0]))
            .toList();
    }
}
