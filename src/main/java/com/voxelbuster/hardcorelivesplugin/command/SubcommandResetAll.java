package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

public class SubcommandResetAll extends PluginSubcommand {
    protected SubcommandResetAll(HardcoreLivesPlugin plugin) {
        super("resetAll", Optional.of("hardcorelives.resetAll"), plugin);
        this.description = "Resets the player data for every player. This includes offline players.";
        this.usage = "/hl resetAll";
        this.aliases = Collections.singletonList("resetall");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        try {
            plugin.getConfigManager().wipePlayers(plugin.getPlayersDir());
            sender.sendMessage(ChatColor.LIGHT_PURPLE + "All Hardcore Lives player data wiped.");
            plugin.updateScoreboard();
            return true;
        } catch (IOException e) {
            log.log(Level.SEVERE, ChatColor.RED + "Could not wipe players!", e);
            return false;
        }
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return List.of();
    }
}
