package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PluginBaseCommand extends Command implements CommandExecutor, TabCompleter {

    private final EnumMap<SubCommandId, PluginSubcommand> subcommands = new EnumMap<>(SubCommandId.class);

    protected final Logger log;
    final List<String> commandAliases = Arrays.asList("hardcorelives", "hl", "hlives");
    private final HardcoreLivesPlugin plugin;

    public PluginBaseCommand(String name, HardcoreLivesPlugin plugin) {
        super(name);
        this.plugin = plugin;
        this.log = plugin.getLogger();
        this.description = new StringBuilder()
            .append(ChatColor.AQUA)
            .append("Command that contains all functionality of HardcoreLives")
            .append(ChatColor.GREEN)
            .append(" /hl")
            .append(ChatColor.AQUA)
            .append(" or")
            .append(ChatColor.GREEN)
            .append(" /hl help to see subcommands.")
            .toString();

        for (SubCommandId subCommandId : SubCommandId.values()) {
            subcommands.put(subCommandId, subCommandId.getSubcommand().apply(plugin));
        }

        this.usageMessage = ChatColor.GREEN + "/hl <subcommand>";
        this.setAliases(Arrays.asList("hl", "hardcorelives", "hlives"));
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String alias,
                             String[] args) {
        return execute(commandSender, alias, args);
    }

    @Override
    public boolean execute(@NotNull CommandSender commandSender, String alias, String[] args) {
        if (!commandAliases.contains(alias.toLowerCase())) {
            return false;
        }

        if (args.length == 0) {
            return new SubcommandHelp(plugin).execute(commandSender, "help", new String[0]);
        }

        SubCommandId subCommandsMapKey = SubCommandId.subCommandByAlias(args[0].toLowerCase());

        if (!subcommands.containsKey(subCommandsMapKey)) {
            commandSender.sendMessage(ChatColor.RED + "Invalid subcommand. " + ChatColor.GREEN +
                "/hl help " + ChatColor.RED + "for help.");
            return false;
        }

        return subcommands
            .getOrDefault(
                subCommandsMapKey,
                new SubcommandHelp(plugin)
            )
            .execute(commandSender, args[0], getSubCommandArgs(args));
    }

    private static @NotNull String[] getSubCommandArgs(@NotNull String[] args) {
        if (args.length == 1) {
            return new String[0];
        }

        return Arrays.copyOfRange(args, 1, args.length);
    }

    @Override
    public @NotNull List<String> tabComplete(@NotNull CommandSender commandSender, String alias,
                                             String[] args) throws IllegalArgumentException {
        if (!commandAliases.contains(alias.toLowerCase())) {
            return super.tabComplete(commandSender, alias, args);
        }

        if (args.length == 1) {
            return SubCommandId.getAllAliases().stream().filter(s -> s.startsWith(args[0].toLowerCase()))
                .toList();
        }

        SubCommandId subCommandsMapKey = SubCommandId.subCommandByAlias(args[0].toLowerCase());

        if (!subcommands.containsKey(subCommandsMapKey)) {
            return Collections.emptyList();
        }

        return subcommands.get(subCommandsMapKey)
            .tabComplete(commandSender, args[0], getSubCommandArgs(args));
    }

    @Override
    public List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command,
                                      @NotNull String alias, String[] args) {
        return tabComplete(commandSender, alias, args);
    }

    enum SubCommandId {
        NONE(SubcommandHelp::new, "", null),
        HELP(SubcommandHelp::new, "help", "h"),
        LIVES(SubcommandLives::new, "lives", "l"),
        RESET(SubcommandReset::new, "reset"),
        RESETALL(SubcommandResetAll::new, "resetall"),
        RELOAD(SubcommandReload::new, "reload"),
        CONFIG(SubcommandConfig::new, "config", "c"),
        SETLIVES(SubcommandSetLives::new, "setlives", "sl"),
        ADDLIVES(SubcommandAddLives::new, "addlives", "al"),
        SETMAX(SubcommandSetMax::new, "setmax", "sm"),
        GIVELIVES(SubcommandGiveLives::new, "givelives", "gl"),
        SCOREBOARDSLOT(SubcommandScoreboardSlot::new, "scoreboardslot", "ss"),
        SCOREBOARD(SubcommandScoreboard::new, "scoreboard", "scoreboarddisplay", "sd"),
        SAVE(SubcommandSave::new, "save"),
        AUTOSAVE(SubcommandAutosave::new, "autosave");

        private final List<String> aliases;

        private final Function<HardcoreLivesPlugin, ? extends PluginSubcommand> pluginSubcommandConstructor;

        SubCommandId(
            Function<HardcoreLivesPlugin, ? extends PluginSubcommand> pluginSubcommandConstructor,
                     String... aliases
        ) {
            this.pluginSubcommandConstructor = pluginSubcommandConstructor;
            this.aliases = Arrays.stream(aliases).filter(Objects::nonNull).toList();
        }

        @NotNull
        public static PluginBaseCommand.SubCommandId subCommandByAlias(String s) {
            for (SubCommandId subCommandId : values()) {
                if (subCommandId.hasAlias(s)) {
                    return subCommandId;
                }
            }

            return NONE;
        }

        public static List<String> getAllAliases() {
            ArrayList<String> aliases = new ArrayList<>();
            for (SubCommandId subCommandId : values()) {
                aliases.addAll(subCommandId.aliases.stream().filter(s -> s != null && !s.isEmpty())
                    .collect(Collectors.toCollection(ArrayList::new)));
            }
            return aliases;
        }

        public boolean hasAlias(String s) {
            return this.aliases.contains(s);
        }

        @SuppressWarnings("unchecked")
        public Function<HardcoreLivesPlugin, PluginSubcommand> getSubcommand() {
            return (Function<HardcoreLivesPlugin, PluginSubcommand>) pluginSubcommandConstructor;
        }
    }
}
