package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class SubcommandReload extends PluginSubcommand {
    protected SubcommandReload(HardcoreLivesPlugin plugin) {
        super("reload", Optional.of("hardcorelives.reload"), plugin);
        this.description = "Reloads the plugin and player data from the config.";
        this.usage = "/hl reload";
        this.aliases = Collections.singletonList("reload");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        if (args.length < 1) {
            plugin.reload(false);
        } else {
            plugin.reload(Boolean.parseBoolean(args[0]) || args[0].equals("1"));
        }

        sender.sendMessage(ChatColor.GREEN + "HardcoreLives reload complete.");
        return true;
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return List.of();
    }
}
