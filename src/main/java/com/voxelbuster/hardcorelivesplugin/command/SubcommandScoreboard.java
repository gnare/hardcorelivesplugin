package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.event.PlayerDisplayLifeScoreboardEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SubcommandScoreboard extends PluginSubcommand {
    protected SubcommandScoreboard(HardcoreLivesPlugin plugin) {
        super("scoreboard", Optional.of("hardcorelives.scoreboard"), plugin);
        this.description = "Toggle the scoreboard for yourself.";
        this.usage = "/hl scoreboard";
        this.aliases = Arrays.asList("scoreboard", "scoreboarddisplay", "sd");
    }

    @Override
    @SuppressWarnings("squid:S3516") // parent method can return false
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        if (!(sender instanceof Player p)) {
            sender.sendMessage(ChatColor.RED + "You must be a player.");
            return true;
        }

        if (p.getScoreboard() != plugin.getScoreboard()) {
            sender.sendMessage(ChatColor.GOLD + "Lives scoreboard on.");
            p.setScoreboard(plugin.getScoreboard());
            plugin.getServer().getPluginManager().callEvent(new PlayerDisplayLifeScoreboardEvent(p));
            return true;
        }

        sender.sendMessage(ChatColor.GOLD + "Lives scoreboard off.");
        p.setScoreboard(Objects.requireNonNull(Bukkit.getScoreboardManager()).getMainScoreboard());
        return true;
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return List.of();
    }
}
