package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class SubcommandReset extends PluginSubcommand {
    protected SubcommandReset(HardcoreLivesPlugin plugin) {
        super("resetAll", Optional.of("hardcorelives.reset"), plugin);
        this.description = "Resets the player data for a single player.";
        this.usage = "/hl reset <player>";
        this.aliases = Collections.singletonList("reset");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length != 1) {
            sendUsageMessage(sender);
            return false;
        }

        String playerName = args[0];

        @SuppressWarnings("deprecation")
        OfflinePlayer p = plugin.getServer().getOfflinePlayer(playerName);
        if (!p.hasPlayedBefore()) {
            sender.sendMessage(ChatColor.RED + "Player not found.");
            return true;
        }

        configManager.setPlayerData(p, new ConfigManager.PlayerData(p, configManager.getConfig()));
        sender.sendMessage(
            ChatColor.LIGHT_PURPLE + "Hardcore Lives player " + p.getName() + " data wiped.");

        plugin.getExecutor().submit(() -> trySavePlayerAfterReset(configManager, p));

        plugin.updateScoreboard();

        return true;
    }

    private void trySavePlayerAfterReset(ConfigManager configManager, OfflinePlayer p) {
        try {
            configManager.savePlayer(p, plugin.getPlayersDir());
        } catch (IOException e) {
            log.severe("Failed to save player data for " + p.getName() + " after reset: " + e.getMessage());
        }
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            return List.of();
        }

        ArrayList<String> playerNames = new ArrayList<>();
        plugin.getServer().getOnlinePlayers().forEach(p -> playerNames.add(p.getName()));
        return playerNames.stream().filter(s -> s.startsWith(args[0])).toList();
    }
}
