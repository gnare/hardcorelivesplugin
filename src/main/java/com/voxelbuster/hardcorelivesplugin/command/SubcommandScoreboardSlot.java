package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SubcommandScoreboardSlot extends PluginSubcommand {
    protected SubcommandScoreboardSlot(HardcoreLivesPlugin plugin) {
        super("scoreboardSlot", Optional.of("hardcorelives.scoreboard.manage"), plugin);
        this.description = "Set the scoreboard display slot for the plugin. This change is for every player.";
        this.usage = "/hl scoreboardSlot <SIDEBAR/BELOW_NAME/PLAYER_LIST>";
        this.aliases = Arrays.asList("scoreboardslot", "ss");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length != 1) {
            sendUsageMessage(sender);
            return false;
        }

        DisplaySlot ds;
        try {
            ds = DisplaySlot.valueOf(args[0].toUpperCase());
        } catch (IllegalArgumentException e) {
            sender.sendMessage(ChatColor.RED + "Invalid display slot type.");
            return true;
        }

        configManager.getConfig().setScoreboardDisplaySlot(ds);
        plugin.getScoreboardObjective().setDisplaySlot(ds);
        plugin.updateScoreboard();
        sender.sendMessage("Scoreboard slot updated.");

        return true;
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (!aliases.contains(alias.toLowerCase())) {
            return List.of();
        }

        if (args.length != 1) {
            return List.of();
        }

        List<String> optionNames =
            Arrays.stream(DisplaySlot.values())
                .map(Enum::toString)
                .toList();

        return optionNames.stream()
            .filter(s -> s.toUpperCase().startsWith(args[0]))
            .toList();
    }
}
