package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.scoreboard.DisplaySlot;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class SubcommandConfig extends PluginSubcommand {

    private final List<String> keyOptionNames = Arrays
        .asList("allowTotemOfUndying", "spectateWhenNoMoreLives", "startingLives",
            "allowBuyingLives", "allowSellingLives", "allowGivingLives",
            "lifeBuyPrice", "lifeSellPrice", "scoreboardDisplaySlot");

    protected SubcommandConfig(HardcoreLivesPlugin plugin) {
        super("config", Optional.of("hardcorelives.config"), plugin);
        this.description = "Set the value of a config variable. Takes effect immediately. " +
            "This will also overwrite the config! Changing the autosave configuration is not permitted with this" +
            " command. Use " + ChatColor.GREEN + "/hl autosave" + ChatColor.RESET + " instead.";
        this.usage = "/hl config <key> <value>";
        this.aliases = Arrays.asList("config", "c");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length != 2) {
            sendUsageMessage(sender);
            return false;
        }

        try {
            configParameterHandler(sender, args, configManager);
        } catch (IllegalArgumentException e) {
            sender.sendMessage(ChatColor.RED + "Invalid argument.");
            return false;
        }

        plugin.getLogger().log(Level.INFO, "Saving config...");
        trySaveConfig(configManager);
        return true;
    }

    private static void configParameterHandler(CommandSender sender, String[] args, ConfigManager configManager) {
        switch (args[0]) {
            case "allowTotemOfUndying" ->
                configManager.getConfig().setAllowTotemOfUndying(parseBoolean(args[1]));
            case "spectateWhenNoMoreLives" ->
                configManager.getConfig().setSpectateWhenNoMoreLives(parseBoolean(args[1]));
            case "startingLives" -> configManager.getConfig().setStartingLives(parseInt(args[1]));
            case "allowBuyingLives" ->
                configManager.getConfig().setAllowBuyingLives(parseBoolean(args[1]));
            case "allowSellingLives" ->
                configManager.getConfig().setAllowSellingLives(parseBoolean(args[1]));
            case "allowGivingLives" ->
                configManager.getConfig().setAllowGivingLives(parseBoolean(args[1]));
            case "loseLifeOnPvpOnly" ->
                configManager.getConfig().setLoseLifeOnPvpOnly(parseBoolean(args[1]));
            case "lifeBuyPrice" -> configManager.getConfig().setLifeBuyPrice(parseFloat(args[1]));
            case "lifeSellPrice" -> configManager.getConfig().setLifeSellPrice(parseFloat(args[1]));
            case "scoreboardDisplaySlot" ->
                configManager.getConfig().setScoreboardDisplaySlot(DisplaySlot.valueOf(args[1]));
            default -> sender.sendMessage(ChatColor.RED + "Invalid config option.");
        }
    }

    private void trySaveConfig(ConfigManager configManager) {
        try {
            configManager.saveConfig(plugin.getConfigFile());
        } catch (IOException e) {
            log.log(Level.SEVERE, ChatColor.RED + "Failed to save config!", e);
        }
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            return List.of();
        }

        return keyOptionNames.stream().filter(s -> s.startsWith(args[0])).toList();
    }
}
