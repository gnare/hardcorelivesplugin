package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import com.voxelbuster.hardcorelivesplugin.event.PlayerLifeCountChangedEvent;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SubcommandLives extends PluginSubcommand {
    protected SubcommandLives(HardcoreLivesPlugin plugin) {
        super("lives", Optional.empty(), plugin);

        this.description = "Show the number of lives for yourself or another player.";
        this.usage = "/hl lives " + ChatColor.YELLOW + "[player (default: yourself)]";
        this.aliases = Arrays.asList("lives", "l");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        if (args.length < 1) {
            if (sender instanceof Player p) {
                ConfigManager.PlayerData data = plugin.getConfigManager().getPlayerData(p);
                if (data.getLives() == 1) {
                    p.sendMessage(
                        ChatColor.YELLOW + String.format("You have %d more life remaining.", data.getLives()));
                } else {
                    p.sendMessage(
                        ChatColor.YELLOW + String.format("You have %d more lives remaining.", data.getLives()));
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
                return false;
            }
        } else if (args.length == 1) {
            if (PermissionUtil.hasPermission(sender, "hardcorelives.lives.other")) {
                Player p = plugin.getServer().getPlayer(args[0]);
                if (p == null) {
                    sender.sendMessage(ChatColor.RED + "Player not found.");
                } else {
                    ConfigManager.PlayerData data = plugin.getConfigManager().getPlayerData(p);
                    if (data.getLives() == 1) {
                        sender.sendMessage(
                            ChatColor.YELLOW + String
                                .format("%s has %d more life remaining.", p.getName(), data.getLives()));
                    } else {
                        sender.sendMessage(
                            ChatColor.YELLOW + String
                                .format("%s has %d more lives remaining.", p.getName(), data.getLives()));
                    }
                    return true;
                }
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else if (args.length == 2) {
            if (sender instanceof Player) {
                ConfigManager configManager = plugin.getConfigManager();
                if (HardcoreLivesPlugin.getEcon() == null) {
                    sender.sendMessage(ChatColor.DARK_RED + "Economy not loaded.");
                    return false;
                }
                if (args[0].equalsIgnoreCase("buy")) {
                    if (!configManager.getConfig().isAllowBuyingLives()) {
                        sender.sendMessage(ChatColor.DARK_RED + "This command is disabled.");
                        sender.sendMessage(
                            ChatColor.YELLOW + "If you are an admin and this is incorrect, check your config " +
                                "values.");
                        return false;
                    }

                    if (PermissionUtil.hasPermission(sender, "hardcorelives.lives.buy")) {
                        Player p = (Player) sender;
                        ConfigManager.PlayerData data = configManager.getPlayerData(p);

                        int numLives = Integer.parseInt(args[1]);
                        float buyprice = configManager.getConfig().getLifeBuyPrice();

                        if (numLives < 1) {
                            sender.sendMessage("You cannot buy less than 1 life.");
                            return false;
                        }

                        EconomyResponse response = HardcoreLivesPlugin.getEcon()
                            .withdrawPlayer(p, buyprice * numLives);

                        if (response.type == EconomyResponse.ResponseType.SUCCESS) {
                            data.setLives(data.getLives() + numLives);
                            plugin.getServer().getPluginManager()
                                .callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                            p.sendMessage(ChatColor.GREEN + String.format("Purchased %d lives.", numLives));
                            plugin.updateScoreboard();
                        } else {
                            sender.sendMessage(ChatColor.RED + String.format("Insufficient funds. " +
                                    "The price for %d lives is %.2f. (%dx%.2f)", numLives,
                                buyprice * numLives,
                                numLives,
                                buyprice));
                            return false;
                        }

                        if (data.getLives() == 1) {
                            p.sendMessage(
                                ChatColor.YELLOW + String
                                    .format("You have %d more life remaining.", data.getLives()));
                        } else {
                            p.sendMessage(
                                ChatColor.YELLOW + String
                                    .format("You have %d more lives remaining.", data.getLives()));
                        }
                    } else {
                        sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                        return false;
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("sell")) {
                    if (!configManager.getConfig().isAllowSellingLives()) {
                        sender.sendMessage(ChatColor.DARK_RED + "This command is disabled.");
                        sender.sendMessage(
                            ChatColor.YELLOW + "If you are an admin and this is incorrect, check your config " +
                                "values.");
                        return false;
                    }

                    if (PermissionUtil.hasPermission(sender, "hardcorelives.lives.sell")) {
                        Player p = (Player) sender;
                        ConfigManager.PlayerData data = configManager.getPlayerData(p);

                        int numLives = Integer.parseInt(args[1]);
                        float sellprice = configManager.getConfig().getLifeSellPrice();

                        if (numLives < data.getLives() && numLives > 0) {
                            HardcoreLivesPlugin.getEcon().depositPlayer(p, sellprice * numLives);
                            data.setLives(data.getLives() - numLives);
                            plugin.getServer().getPluginManager()
                                .callEvent(new PlayerLifeCountChangedEvent(p, data.getLives()));
                            p.sendMessage(ChatColor.GREEN + String.format("Sold %d lives.", numLives));
                            plugin.updateScoreboard();
                        } else {
                            sender.sendMessage("You cannot sell more lives than you have or less than 1 life.");
                            return false;
                        }

                        if (data.getLives() == 1) {
                            p.sendMessage(
                                ChatColor.YELLOW + String
                                    .format("You have %d more life remaining.", data.getLives()));
                        } else {
                            p.sendMessage(
                                ChatColor.YELLOW + String
                                    .format("You have %d more lives remaining.", data.getLives()));
                        }
                    } else {
                        sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                        return false;
                    }
                    return true;
                }
            } else {
                sender.sendMessage(ChatColor.RED + "You must be a player to use this command!");
                return false;
            }
        }
        sendUsageMessage(sender);
        return false;
    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length == 1) {
                ArrayList<String> optionNames = new ArrayList<>();
                plugin.getServer()
                    .getOnlinePlayers()
                    .forEach(p -> optionNames.add(p.getName()));

                optionNames.addAll(Arrays.asList("buy", "sell"));
                return optionNames.stream()
                    .filter(s -> s.startsWith(args[0]))
                    .collect(Collectors.toList());
            } else {
                return List.of();
            }
        } else {
            return List.of();
        }
    }
}
