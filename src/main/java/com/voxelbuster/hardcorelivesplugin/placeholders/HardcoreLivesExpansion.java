package com.voxelbuster.hardcorelivesplugin.placeholders;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public class HardcoreLivesExpansion extends PlaceholderExpansion {
    private final HardcoreLivesPlugin plugin;

    public HardcoreLivesExpansion(HardcoreLivesPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public @NotNull String getIdentifier() {
        return plugin.getName().toLowerCase();
    }

    @Override
    public @NotNull String getAuthor() {
        return "VoxelBuster";
    }

    @Override
    public @NotNull String getVersion() {
        return plugin.getDescription().getVersion();
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public @Nullable String onRequest(OfflinePlayer player, @NotNull String params) {
        if (params.equalsIgnoreCase("lives")) {
            try {
                plugin.getConfigManager().loadPlayerData(player, plugin.getPlayersDir());
                return String.valueOf(plugin.getConfigManager().getPlayerData(player).getLives());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else if (params.equalsIgnoreCase("max_lives")) {
            return String.valueOf(plugin.getConfigManager().getConfig().getStartingLives());
        }

        return super.onRequest(player, params);
    }
}
