package com.voxelbuster.hardcorelivesplugin;

import lombok.Setter;
import org.bukkit.scheduler.BukkitTask;

import java.io.IOException;

import static java.util.logging.Level.SEVERE;

public class SaveTaskScheduler {
    private final HardcoreLivesPlugin plugin;

    private final Runnable saveTask;

    @Setter
    private long saveInterval = 0;

    private BukkitTask currentTaskHandle;

    public SaveTaskScheduler(HardcoreLivesPlugin plugin) {
        this.plugin = plugin;

        saveTask = () -> {
            try {
                plugin.getConfigManager().saveAllPlayers(plugin.getPlayersDir());
            } catch (IOException e) {
                plugin.log.log(SEVERE, "Saving player data failed!", e);
            }
        };
    }

    public void rescheduleTask() {
        if (currentTaskHandle != null) {
            currentTaskHandle.cancel();
        }

        if (saveInterval > 0) {
            this.currentTaskHandle = plugin.getServer().getScheduler()
                .runTaskTimer(plugin, saveTask, saveInterval, saveInterval);
        }
    }
}
