package com.voxelbuster.hardcorelivesplugin;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PermissionUtil {
    private static net.milkbowl.vault.permission.Permission permissionManager;

    public static void registerPermissions() {
        PluginManager pm = Bukkit.getPluginManager();
        ArrayList<Permission> permsList = new ArrayList<>();

        permsList.add(new Permission("hardcorelives.bypass"));
        permsList.add(new Permission("hardcorelives.lives.others"));
        permsList.add(new Permission("hardcorelives.lives.buy"));
        permsList.add(new Permission("hardcorelives.lives.sell"));
        permsList.add(new Permission("hardcorelives.lives.give"));
        permsList.add(new Permission("hardcorelives.reset"));
        permsList.add(new Permission("hardcorelives.resetAll"));
        permsList.add(new Permission("hardcorelives.reload"));
        permsList.add(new Permission("hardcorelives.config"));
        permsList.add(new Permission("hardcorelives.setLives"));
        permsList.add(new Permission("hardcorelives.scoreboard"));
        permsList.add(new Permission("hardcorelives.scoreboard.manage"));

        permsList.forEach(pm::addPermission);
    }

    public static void setPermissionManager(net.milkbowl.vault.permission.Permission permissionManager) {
        PermissionUtil.permissionManager = permissionManager;
    }

    public static boolean hasPermission(CommandSender sender, @NotNull String node) {
        return sender.hasPermission(node) || (permissionManager != null && permissionManager.has(sender, node));
    }
}
