package com.voxelbuster.hardcorelivesplugin;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import me.clip.placeholderapi.util.FileUtil;
import org.bukkit.OfflinePlayer;
import org.bukkit.scoreboard.DisplaySlot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.Stream;

@SuppressWarnings("UnusedReturnValue")
public class ConfigManager {
    private final HashMap<OfflinePlayer, PlayerData> playerDataMap = new HashMap<>();
    private final HardcoreLivesPlugin plugin;
    @Getter
    private Config config;

    public ConfigManager(HardcoreLivesPlugin plugin) {
        this.plugin = plugin;
    }

    public boolean loadConfig(File configFile) throws IOException {
        if (generateConfig(configFile)) {
            return true;
        } else {
            Gson gson = new Gson();
            FileReader fr = new FileReader(configFile);
            try {
                this.config = gson.fromJson(fr, Config.class);
            } catch (JsonSyntaxException e) {
                plugin.log.log(Level.SEVERE, "Bad JSON in config, resetting.", e);
                generateConfig(configFile);
                return false;
            }
            return false;
        }
    }

    public boolean generateConfig(File configFile) throws IOException {
        if (configFile.createNewFile()) {
            FileWriter fw = new FileWriter(configFile);
            this.config = new Config();
            Gson gson = new Gson();
            gson.toJson(config, fw);
            fw.close();
            return true;
        }

        return false;
    }

    public void saveConfig(File configFile) throws IOException {
        FileWriter fw = new FileWriter(configFile);
        Gson gson = new Gson();
        gson.toJson(config, fw);
        fw.close();
    }

    public void setPlayerData(OfflinePlayer p, PlayerData data) {
        this.playerDataMap.put(p, data);
    }

    public void saveAllPlayers(File playersDir) throws IOException {
        for (OfflinePlayer p : playerDataMap.keySet()) {
            savePlayer(p, playersDir);
        }
    }

    public boolean savePlayer(OfflinePlayer p, File playersDir) throws IOException {
        if (!playerDataMap.containsKey(p)) {
            return false;
        } else {
            Gson gson = new Gson();
            FileWriter fw = new FileWriter(
                Paths.get(playersDir.toString(), p.getUniqueId() + ".json").toString());
            gson.toJson(playerDataMap.get(p), fw);
            fw.close();
            return true;
        }
    }

    public void wipePlayers(File playersDir) throws IOException {
        OfflinePlayer[] loadedPlayers = getLoadedPlayers();
        unloadAllPlayers(playersDir);

        Path oldPlayersDir = Paths.get(playersDir.getParentFile().toPath().toString(), "players.old");
        recursivelyDelete(oldPlayersDir);

        Files.move(playersDir.toPath(), oldPlayersDir,
            StandardCopyOption.REPLACE_EXISTING);
        // noinspection ResultOfMethodCallIgnored
        playersDir.mkdir();
        playerDataMap.clear();

        for (OfflinePlayer p : loadedPlayers) {
            loadPlayerData(p, playersDir);
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void recursivelyDelete(Path directory) {
        if (!Files.isDirectory(directory)) {
            return;
        }
        try (Stream<Path> paths = Files.walk(directory)) {
            paths.sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
        } catch (IOException e) {
            plugin.log.severe("Failed to delete directory: " + directory);
        }
    }

    public OfflinePlayer[] getLoadedPlayers() {
        return playerDataMap.keySet().toArray(new OfflinePlayer[0]);
    }

    public void unloadAllPlayers(File playersDir) throws IOException {
        for (OfflinePlayer p : playerDataMap.keySet()) {
            unloadPlayer(p, playersDir);
        }
    }

    /**
     * Loads the PlayerData into the internal Map. Returns true if the file was
     * created and false if it already exists.
     *
     * @param p          the player
     * @param playersDir player data storage directory
     * @return true if existing data was loaded, false if a new save had to be
     * generated
     * @throws IOException if a file could not be written
     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean loadPlayerData(OfflinePlayer p, File playersDir) throws IOException {
        if (!playersDir.isDirectory()) {
            throw new FileNotFoundException("playersDir must be a directory!");
        } else {
            File playerFile = new File(
                Paths.get(playersDir.toString(), p.getUniqueId() + ".json").toString());
            if (playerFile.createNewFile()) {
                PlayerData playerData = new PlayerData(p, this.getConfig());
                playerDataMap.put(p, playerData);
                return true;
            } else {
                Gson gson = new Gson();
                FileReader fr = new FileReader(playerFile);
                try {
                    PlayerData pd = gson.fromJson(fr, PlayerData.class);
                    playerDataMap.put(p, pd);
                } catch (JsonSyntaxException e) {
                    plugin.log.log(Level.SEVERE, "Bad JSON in config, resetting.", e);
                    playerDataMap.put(p, new PlayerData(p, this.getConfig()));
                }
                fr.close();
                return false;
            }
        }
    }

    public boolean unloadPlayer(OfflinePlayer p, File playersDir) throws IOException {
        if (!playerDataMap.containsKey(p)) {
            return false;
        } else {
            Gson gson = new Gson();
            FileWriter fw = new FileWriter(
                Paths.get(playersDir.toString(), p.getUniqueId() + ".json").toString());
            gson.toJson(playerDataMap.remove(p), fw);
            fw.close();
            return true;
        }
    }

    public PlayerData getPlayerData(OfflinePlayer p) {
        return playerDataMap.get(p);
    }

    @Getter
    @Setter
    @NoArgsConstructor
    public static class Config implements Serializable {
        private int startingLives = 3;
        private int defaultMaxLives = 3;
        private float lifeBuyPrice = 10000;
        private float lifeSellPrice = 5000;
        private int autosaveInterval = 5;
        private TimeUnit autosaveUnit = TimeUnit.MINUTES;
        private boolean allowBuyingLives = false;
        private boolean allowSellingLives = false;
        private boolean allowGivingLives = false;
        private boolean allowTotemOfUndying = true;
        private boolean spectateWhenNoMoreLives = true;
        private boolean loseLifeOnPvpOnly = false;
        private DisplaySlot scoreboardDisplaySlot = DisplaySlot.BELOW_NAME;
    }

    @SuppressWarnings("unused")
    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    public static class PlayerData implements Serializable {
        private UUID uuid;
        private String username;
        private int lives;
        private boolean bypassLives;

        private int maxLives;

        public PlayerData(OfflinePlayer p, Config c) {
            this(p, c.getStartingLives(), false, c.getDefaultMaxLives());
        }

        public PlayerData(OfflinePlayer p, int lives, boolean bypassLives, int maxLives) {
            this.uuid = p.getUniqueId();
            this.username = p.getName();
            this.bypassLives = bypassLives;
            this.lives = lives;
            this.maxLives = maxLives;
        }

        public void setLives(int lives) {
            int maxLives = (this.maxLives > 0) ? this.maxLives : Integer.MAX_VALUE;
            this.lives = Math.max(Math.min(lives, maxLives), 0);
        }

        public void setMaxLives(int maxLives) {
            this.maxLives = Math.max(maxLives, 0);

            if (maxLives < lives && maxLives > 0) {
                setLives(maxLives);
            }
        }
    }
}
